# Compassion Game

By Compassion Planet <img height="30px" alt="Compassion Planet logo" src="static/img/compassion-planet-logo.png" style="max-height:30px; vertical-align:bottom;"/>  
<br>
<img width="50%" src="docs/assets/compassiongame-home-2022-09-12.png" style="max-height:350px; display:block; margin: 0 auto">  

## Architecture

[Django](https://www.djangoproject.com/) - [Postgres](https://www.postgresql.org/) - [Bootstrap](https://getbootstrap.com/) -
[Celery](https://docs.celeryq.dev/en/stable/index.html) - [Redis](https://redis.io/) - [nginx](https://nginx.org/en/) -
[Linux](https://www.linuxfoundation.org/)  

<img height="50px" src="docs/assets/django.png" />
<img height="50px" src="docs/assets/psql.png" style="max-height:50px; max-width=50px;"/>
<img height="50px" src="docs/assets/bootstrap5.png" style="max-height:50px; max-width=50px;"/>
<img height="50px" src="docs/assets/celery-logo.png" style="max-height:50px; max-width=50px;"/>
<img height="50px" src="docs/assets/redis-logo.png" style="max-height:50px; max-width=50px;"/>
<img height="50px" src="docs/assets/nginx.png" style="max-height:50px; max-width=50px;"/>
<img height="50px" src="docs/assets/linux-penguin.png" style="max-height:50px; max-width=50px;"/>

## Quickstart

1. Recommend using a Unix based OS:  
    macOS, Windows Subsystem for Linux, or Linux distro
1. Install OS dependencies:  

    ```sh
    # These dependency names are for Arch pacman manager.
    # They might be different based on your OS package manager.
    python-dev python-pip python-virtualenv gcc libpq-dev postgresql git
    ```

1. `git clone git@gitlab.com:evanlesmez/compassion-planet-game.git`
1. Start postgresql service  
    [ArchWiki Postgres setup guide](https://wiki.archlinux.org/title/PostgreSQL#Installation)  
    [Ubuntu Postgres setup guide](https://ubuntu.com/server/docs/databases-postgresql)  

1. Start psql shell as postgres admin

    ```sh
    sudo -u postgres psql
    ```

1. Create `compassion_game` database

    ```sh
    CREATE DATABASE compassion_game;
    ```

1. Create project user

    ```sh
    CREATE USER <user> WITH PASSWORD '<password>';
    ```

    - Make note of the username and password you create for later.  

    ```sh
    ALTER ROLE myprojectuser SET client_encoding TO 'utf8';
    ALTER ROLE myprojectuser SET default_transaction_isolation TO 'read committed';
    ALTER ROLE myprojectuser SET timezone TO 'UTC';
    GRANT ALL PRIVILEGES ON DATABASE "compassion_game" TO <user>;
    ```  

    Exit psql prompt with  

    ```sh
    \q
    ```

1. Install [Poetry](https://python-poetry.org/) for Python dependency management

    ```sh
    curl -sSL https://install.python-poetry.org | python3 -
    ```

1. Move to project root

    ```sh
    cd compassion-planet-game 
    ```

1. Install python dependencies

    ```sh
    poetry install
    ```

1. Copy secrets example file

    ```sh
    cp cplanetsecrets.example.py cplanetsecrets.py
    ```

1. Fill in needed configuration for dev
    1. Dev Database

        ```py
        DEV_DJANGO_DB_CONF = {
            "default": {
                "ENGINE": "django.db.backends.postgresql",
                "NAME": "compassion_game",
                "HOST": "localhost",
                "USER": "<created compassion_game user >",
                "PASSWORD": "<created compassion_game password>",
                "PORT": "5432",
            },
        }
        ```

    1. `SECRET_KEY`  
        [Generate a secret key for Django](https://django-secret-key-generator.netlify.app/) and place in `SECRET_KEY`

1. Enter poetry environment

    ```sh
    poetry shell
    ```

2. Make db migrations

    ```sh
    python manage.py migrate
    ```

3. Create Django superuser

    ```sh
    python manage.py createsuperuser
    ```

4. Download and extract the [media folder](https://drive.google.com/file/d/1xO1OchV4NsXi9nVGiD1kpAROVJiz4Ilg/view?usp=sharing) and put it into the root directory of the project

5. Download and replace all 'ev' instances to your db username in the [dump file](https://drive.google.com/file/d/1Uri9wN-JLjekhiVUnkTbQnIE0aTrleE2/view?usp=sharing)
   
    for windows: run


        "C:\Program Files\PostgreSQL\14\bin\psql.exe" -d compassion_game -U root < "<path_to_file>\COMPASSION_GAME_BAK"

    for others:

        psql compassion_game < <path_to_file>/compassion_game_bak
 

6. Run the server locally

    ```sh
    python manage.py runserver
    ```

7. Go to local admin:
    <http://localhost:8000/admin>

8. Create at least one avatar (aka starter animal).  
    Upload a placeholder image.  

    <http://localhost:8000/admin/game/avatar/>

9.  Create a player and link it to the superuser
    <http://localhost:8000/admin/game/player/>

10. Go to Compassion Game homepage <http://localhost:8000/>

You are all set!

### Next steps  

- Add placeholder measurables in admin
  - __Measurable__  
    A resource that can be measured with a conversion factor based on vegan equivalent days.  
- Add placeholder badges in admin

## Recommended tools

[__VSCode__](https://code.visualstudio.com/)  
The `.vscode` folder has settings that preconfigure the [black](https://github.com/psf/black) python auto-formatter and Django debugging.  

__Unix based OS__ - macOS, Windows Subsystem for Linux, or Linux distro.  
If you are looking for a fun distro, I (Ev) use [EndeavorOS](https://endeavouros.com/)  

[__pgadmin__](https://www.pgadmin.org/) - DB GUI browser for Postgres.

[__tmux__](https://github.com/tmux/tmux) - If you often find yourself with tons of terminal tabs and windows open at once, like Ev, tmux is your friend.  
It is a neat tool that you can use to setup and manage a grid of terminal panes.  

<img src="docs/assets/ev-tmux-example.jpg" >

## Deployment

__MVP Manual__  
    - [Digital Ocean Ubuntu 22 with Django guide](https://www.digitalocean.com/community/tutorials/how-to-set-up-django-with-postgres-nginx-and-gunicorn-on-ubuntu-22-04)  
        - [Let's Encrypt for HTTPS Digital Ocean Ubuntu 22](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-22-04)  
        - [Digital Ocean DNS quickstart](https://docs.digitalocean.com/products/networking/dns/quickstart/)
        - [UFW Digital Ocean for DB remote access](https://www.digitalocean.com/community/tutorials/ufw-essentials-common-firewall-rules-and-commands)  
        - [Configure Postgres for safe remote access](https://stackoverflow.com/a/38466547/9567831)  
        - [NGINX HTTP max file size](https://linuxhint.com/what-is-client-max-body-size-nginx/)  
    - [Django email setup](https://www.geeksforgeeks.org/setup-sending-email-in-django-project/)

__Docker Container__  
    -[Digital Ocean Container Reg Quickstart](https://docs.digitalocean.com/products/container-registry/quickstart/)
