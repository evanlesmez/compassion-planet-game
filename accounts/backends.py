# app.backends.py
from django.contrib.auth.backends import ModelBackend
from django.db.models import Q
from game.models import User 


class CustomUserModelBackend(ModelBackend):
    def authenticate(self, request, username=None, password=None, **kwargs):
        if username is None:
            username = kwargs.get(User.USERNAME_FIELD, kwargs.get(User.EMAIL_FIELD))

        if username is None or password is None:
            return
        try:
            user = User._default_manager.get(
                # add this  condition after email verification support is implemented & Q(email_verified=True)
                Q(username__exact=username) | (Q(email__iexact=username)) 
            )
        except User.DoesNotExist:
            # Run the default password hasher once to reduce the timing
            # difference between an existing and a nonexistent user (#20760).
            User().set_password(password)
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                return user
