from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


class OurUserCreationForm(UserCreationForm):
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in self.fields.keys():
            self.fields[f].widget.attrs.update({"class": "form-control"})

    class Meta:
        model = get_user_model()
        fields = UserCreationForm.Meta.fields + ("email",)


class EmailForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ('email',)