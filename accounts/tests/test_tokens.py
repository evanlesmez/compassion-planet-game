import pytest
from assertpy import assert_that
from accounts import tokens
from game.models import User
from unittest.mock import create_autospec

MockUser = create_autospec(User)

@pytest.mark.django_db
def test_token_generator():
    user = User.objects.create(username='test', password="password", email="test@mail.com")
    tok = tokens.unsub_email_token_gen.make_token(user)
    assert_that(tokens.unsub_email_token_gen.check_token(tok)).is_equal_to(user)
    m_user = MockUser()
    m_user.email = "pressf@mail.com"
    fake_tok = tokens.unsub_email_token_gen.make_token(user=m_user)
    with pytest.raises(User.DoesNotExist):
        tokens.unsub_email_token_gen.check_token(fake_tok)