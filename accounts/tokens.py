import jwt
from datetime import datetime, timezone, timedelta
from django.contrib.auth import get_user_model, models
from django.conf import settings

# TODO might want to look into salting in future
# It is not the end of the world for unsubscribe and verify account without it
class UnsubEmailTokenGenerator:
    def __init__(self) -> None:
        self.algorithm = "HS256"
        self._email_payload_k = "email"
        self._secret = settings.SECRET_KEY

    def make_token(
        self, user: models.AbstractUser, expire_in_secs: int = 60 * 60 * 24
    ) -> str:
        payload = {
            self._email_payload_k: user.email,
            "exp": datetime.now(tz=timezone.utc) + timedelta(seconds=expire_in_secs),
        }
        return jwt.encode(payload, self._secret, algorithm=self.algorithm)

    def check_token(self, token: str) -> models.AbstractUser:
        payload = jwt.decode(token, self._secret, algorithms=[self.algorithm])
        email = payload[self._email_payload_k]
        user = get_user_model().objects.get(email=email)
        return user


unsub_email_token_gen = UnsubEmailTokenGenerator()
