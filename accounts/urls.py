from django.urls import path
from django.contrib.auth import views as auth_views
from .views import change_email, signup, confirm_email, unsub, resend_verify


urlpatterns = [
    path("signup/", signup, name="signup"),
    path(
        "login/",
        auth_views.LoginView.as_view(redirect_authenticated_user=True),
        name="login",
    ),
    path('email/<str:token>/', confirm_email),
    path('email/unsub/<int:user_id>/<str:token>', unsub, name="unsub_email"),
    path('email/resend_verify', resend_verify, name="resend_verify"),
    path('email/change', change_email, name="change_email")
]
