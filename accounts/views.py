from django.shortcuts import render
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import login
from django.contrib import messages
from django.shortcuts import render
from django_email_verification import send_email
from django_email_verification import verify_email_view, verify_token
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from .tokens import unsub_email_token_gen, jwt
from game.models import Player, User
from .forms import EmailForm, OurUserCreationForm
from game.forms import PlayerCreationForm


def signup(request):
    if request.method == "POST":
        user_form = OurUserCreationForm(request.POST)
        player_form = PlayerCreationForm(request.POST)
        if user_form.is_valid() and player_form.is_valid():
            u = user_form.save()
            p = player_form.save(commit=False)
            p.user = u
            p.save()
            send_email(u)
            login(request, u)
            messages.success(
                request,
                "Thanks for registering! Please check your email to verify your account.",
            )
            return HttpResponseRedirect(reverse("game:starter_animal"))

    else:
        user_form = OurUserCreationForm()
        player_form = PlayerCreationForm()

    return render(
        request,
        "registration/signup.html",
        {"user_form": user_form, "player_form": player_form},
    )


@verify_email_view
def confirm_email(request, token):
    success, user = verify_token(token)
    if success:
        messages.success(request, "Thanks for verifying your email.")
        return HttpResponseRedirect(reverse("home"))

    messages.error(
        request,
        """
        Oops that token is no longer valid.
        Please resend the verification or change your email.
        See 🔝🟦 banner once logged in. 
    """,
    )
    return HttpResponseRedirect(reverse("home"))


def unsub(request, user_id: int, token: str):
    try:
        user = User.objects.get(id=user_id)
        if user == unsub_email_token_gen.check_token(token):
            user.last_login = timezone.now()
            user.is_subscribed_meal_log_mail = False
            user.save()
            messages.success(request, "Your email is unsubscribed.")
        else:
            messages.error(
                """
                The user id did not match the token. 
                Try the link in your most recent email again. 
                Submit feedback if problem persists.
            """
            )

        return HttpResponseRedirect(reverse("home"))

    except jwt.DecodeError:
        messages.error(
            request,
            "The unsubscribe token is not valid. Please try again the email link again.",
        )
        return HttpResponseRedirect(reverse("home"))

    except jwt.ExpiredSignatureError:
        messages.error(
            request,
            "The token is expired for security reasons. Please try the unsubscribe link on the most recent email you received.",
        )
        return HttpResponseRedirect(reverse("home"))

    except User.DoesNotExist:
        messages.error(request, "That user does not exist.")
        return HttpResponseRedirect(reverse("home"))

    except Exception as e:
        return HttpResponse(
            f"""
            Sorry! Your unsubscribe link resulted in a unique error.
            Please copy the content below, login, and paste in the feedback section.
            {repr(e)}
        """
        )


@login_required
def resend_verify(request):
    send_email(request.user)
    return HttpResponse(
        f"Verification email sent to {request.user.email}", content_type="text/plain"
    )


@login_required
def change_email(request):
    if request.method == "POST":
        form = EmailForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            player = Player.objects.get(user=request.user)
            messages.success(request, f"Email changed and sent to {player.user.email}")
            return HttpResponseRedirect(reverse("game:player", args=(player.id,)))
    else:
        form = EmailForm()

    return render(request, "registration/change_email.html", {"form": form})
