from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent
DEV_DJANGO_DB_CONF = {
    "default": {
        "ENGINE": "",
        "NAME": "",
        "HOST": "",
        "USER": "",
        "PASSWORD": "",
        "PORT": "",
    },
}
PROD_DJANGO_DB_CONF = {
    "default": {
        "ENGINE": "",
        "NAME": "",
        "HOST": "",
        "USER": "",
        "PASSWORD": "",
        "PORT": "",
    },
}
SECRET_KEY = ""

ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
ADMINS = [("Yoshi", "yoshi@example.com")]
EMAIL_HOST = "mail or smtp .domain"
EMAIL_PORT = 1
EMAIL_USE_TLS = False
EMAIL_USE_SSL = False
EMAIL_HOST_USER = "yoshi@example.com"
EMAIL_HOST_PASSWORD = "good password"
DEFAULT_FROM_EMAIL = "yoshi@example.com"
SERVER_EMAIL = "yoshi@example.com"
