import os
from celery import Celery
from celery.schedules import crontab


# Set the default Django settings module for the 'celery' program.
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cplanetsite.settings")

app = Celery("cplanetsite")

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object("django.conf:settings", namespace="CELERY")

# Load task modules from all registered Django apps.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    # "debug": {
    #     "task": "game.tasks.debug",
    #     "schedule": 10.0,
    #     "args": ("yo",),
    # },
    "send_log_meals_mail": {
        "task": "game.tasks.send_log_meals_mail",
        "schedule": crontab(hour=21, minute=0, day_of_week="tue,thu,sun"),
    },
}
# TODO get users timezone on login and store in their player profile
# TODO schedule email every hour for users in that timezone (default UTC)
# TODO unit test that game.tasks.send_log is in discovered apps
