from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from game.models import Player
from game.queries import CommunalMeasurable, get_communal_measurables
from typing import List


def home(request):
    if request.user.is_authenticated:
        player = Player.objects.get(user=request.user)
        if player.avatar is None:
            return HttpResponseRedirect(reverse("game:starter_animal"))

        return HttpResponseRedirect(reverse("game:player", args=(player.id,)))

    communal_measurables: List[CommunalMeasurable] = get_communal_measurables()
    return render(request, "home.html", {"communal_measurables": communal_measurables})


def intro(request):
    return HttpResponse("Intro to veganism, compassion planet game, and resources page")
