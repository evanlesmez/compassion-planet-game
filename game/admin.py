from django.contrib import admin
from .models import (
    User,
    DailyMealLog,
    Measurable,
    Badge,
    MeasurableUnit,
    Player,
    PlayerFeedback,
    Avatar,
)

from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

@admin.register(User)
class UserAdmin(BaseUserAdmin):
    fieldsets = BaseUserAdmin.fieldsets + (
            (None, {'fields': ('is_email_verified','is_subscribed_meal_log_mail')}),
    )

admin.site.register(Player)
admin.site.register(PlayerFeedback)
admin.site.register(DailyMealLog)
admin.site.register(Badge)
admin.site.register(Measurable)
admin.site.register(Avatar)
admin.site.register(MeasurableUnit)
