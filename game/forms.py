from .models import DailyMealLog, PlayerFeedback, Player
from django.forms import ModelForm, IntegerField, HiddenInput


class PlayerStarterAnimalForm(ModelForm):
    class Meta:
        model = Player
        fields = ["avatar"]


class PlayerCreationForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for f in self.fields.keys():
            self.fields[f].widget.attrs.update({"class": "form-control"})

    class Meta:
        model = Player
        fields = ["phone_number"]


class DailyMealLogForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["total_meal_count"].widget.attrs.update(
            {"class": "form-control mb-3"}
        )
        self.fields["vegan_meal_count"].widget.attrs.update(
            {"class": "form-control mb-3"}
        )
        self.fields["notes"].widget.attrs.update(
            {"class": "form-control mb-3", "style": "height:100px;"}
        )

    class Meta:
        model = DailyMealLog
        fields = ["total_meal_count", "vegan_meal_count", "notes"]

    def clean(self):
 
        super(DailyMealLogForm, self).clean()
         
        total_meal_count = self.cleaned_data.get('total_meal_count')
        vegan_meal_count = self.cleaned_data.get('vegan_meal_count')
        if vegan_meal_count > total_meal_count:
            self._errors['vegan_meal_count'] = self.error_class([
                'Vegan meal count can\'t be bigger than total meal count.'])
 
        # return any errors if found
        return self.cleaned_data


class PlayerFeedbackForm(ModelForm):
    class Meta:
        model = PlayerFeedback
        fields = ["feedback"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["feedback"].widget.attrs.update(
            {
                "class": "form-control",
                "placeholder": "Leave feedback here please",
                "style": "height: 200px;",
            }
        )
