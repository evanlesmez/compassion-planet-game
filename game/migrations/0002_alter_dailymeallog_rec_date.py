# Generated by Django 4.0.6 on 2022-07-29 21:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailymeallog',
            name='rec_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='date recorded'),
        ),
    ]
