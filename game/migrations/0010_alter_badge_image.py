# Generated by Django 4.0.6 on 2022-08-05 17:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0009_alter_avatar_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='badge',
            name='image',
            field=models.ImageField(upload_to='badges'),
        ),
    ]
