# Generated by Django 4.0.6 on 2022-08-08 18:21

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('game', '0010_alter_badge_image'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playerfeedback',
            name='rec_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='date recorded'),
        ),
    ]
