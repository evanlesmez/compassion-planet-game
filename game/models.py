from email.policy import default
from pyexpat import model
from django.db import models
from pathlib import Path
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField
from django.utils import timezone
from django.http import HttpResponseRedirect
from django.urls import reverse

APP_NAME = Path(__file__).resolve().parent.stem


class User(AbstractUser):
    email = models.EmailField(unique=True)
    is_email_verified = models.BooleanField(default=False)
    is_subscribed_meal_log_mail = models.BooleanField(default=True)

class MeasurableUnit(models.Model):
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name


class Measurable(models.Model):
    class Name(models.TextChoices):
        ANIMAL = "Animal"
        CARBON = "Carbon Emission"
        WATER_USAGE = "Water Usage"
        FOREST_AREA = "Forest Area"

    name = models.CharField(max_length=200, choices=Name.choices)
    rate_per_vegan_day = models.DecimalField(max_digits=10, decimal_places=4)
    unit = models.ForeignKey(MeasurableUnit, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} {self.unit}"


class Avatar(models.Model):
    class Type(models.TextChoices):
        COW = "cow"
        CHICKEN = "chicken"
        PIG = "pig"

    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to="avatars")
    type = models.CharField(max_length=200, choices=Type.choices)
    description = models.TextField(max_length=1000, default="")

    def __str__(self):
        return self.name


class Player(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ForeignKey(Avatar, on_delete=models.CASCADE, null=True)
    phone_number = PhoneNumberField(blank=True, default="")

    def __str__(self):
        return self.user.username


class Badge(models.Model):
    class Name(models.TextChoices):
        ANIMAL = "Animal Lover"
        CARBON = "Carbon"
        WATER_USAGE = "Water"
        FOREST_AREA = "Forest"

    measurable = models.ForeignKey(Measurable, on_delete=models.CASCADE)
    name = models.CharField(max_length=200, choices=Name.choices)
    description = models.CharField(max_length=400)
    image = models.ImageField(upload_to="badges")
    tier = models.IntegerField()
    measurable_threshold_to_earn = models.DecimalField(max_digits=10, decimal_places=4)

    def __str__(self):
        return f"{self.name} {self.tier}"


class PlayerFeedback(models.Model):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    feedback = models.TextField(max_length=1000)
    # disapointment_on_product_disapear score dropdown field
    rec_date = models.DateTimeField("date recorded", default=timezone.now)

    def __str__(self):
        return f"{self.player} at {self.rec_date}"


class DailyMealLog(models.Model):
    class Meta:
        db_table = f"{APP_NAME}_daily_meal_log"

    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    total_meal_count = models.PositiveIntegerField()
    vegan_meal_count = models.PositiveIntegerField()
    notes = models.TextField(max_length=1000, blank=True, default="")
    rec_date = models.DateTimeField("date recorded", default=timezone.now)

    def __str__(self):
        return f"{self.vegan_meal_count}/{self.total_meal_count} vegan score on {self.rec_date} "

    @property
    def was_recorded_today(self) -> bool:
        return timezone.now().date() == self.rec_date.date()
