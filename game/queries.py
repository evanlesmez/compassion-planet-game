from django.db import connection
from collections import namedtuple
from dataclasses import dataclass
from datetime import datetime
from django.utils import timezone
from typing import List, NamedTuple, Dict
import random


@dataclass
class Streak:
    days: int
    start_date: datetime


def get_fully_vegan_streak(player_id: int) -> Streak:
    with connection.cursor() as cursor:
        # fmt: off
        streak = cursor.execute(
            """
            WITH fully_v_streak_groups AS 
            (
                SELECT 
                date_trunc('day', rec_date) as r_date,
                date_trunc('day', rec_date) - INTERVAL '1' DAY * 
                    DENSE_RANK() OVER (ORDER BY date_trunc('day', rec_date)) 
                    as date_minus_row
                FROM game_daily_meal_log
                WHERE player_id = %s 
                    AND total_meal_count = vegan_meal_count 
                GROUP BY date_trunc('day', rec_date)
            ), 
            streaks as (
                SELECT 
                COUNT(*) AS streak,
                MIN(r_date) AS start_date,
                MAX(r_date) AS end_date
                FROM fully_v_streak_groups
                GROUP BY date_minus_row
                ORDER BY end_date DESC
            )
            SELECT streak, start_date FROM streaks
            WHERE end_date <= CURRENT_DATE 
                AND CURRENT_DATE - end_date <= INTERVAL '1' DAY
            LIMIT 1;
            """, [player_id])
        # fmt: on
        result_row = cursor.fetchone()

    if result_row is None:
        return Streak(days=0, start_date=timezone.now())

    return Streak(result_row[0], result_row[1])


def get_partial_vegan_streak(player_id: int) -> Streak:
    with connection.cursor() as cursor:
        # fmt: off
        streak = cursor.execute(
            """
            WITH partial_v_streak_groups AS 
            (
                SELECT 
                date_trunc('day', rec_date) as r_date,
                date_trunc('day', rec_date) - INTERVAL '1' DAY * 
                    DENSE_RANK() OVER (ORDER BY date_trunc('day', rec_date)) 
                    as date_minus_row
                FROM game_daily_meal_log
                WHERE player_id = %s 
                    AND  vegan_meal_count > 0
                    OR total_meal_count = 0
                GROUP BY date_trunc('day', rec_date)
            ), 
            streaks as (
                SELECT 
                COUNT(*) AS streak,
                MIN(r_date) AS start_date,
                MAX(r_date) AS end_date
                FROM partial_v_streak_groups
                GROUP BY date_minus_row
                ORDER BY end_date DESC
            )
            SELECT streak, start_date FROM streaks
            WHERE end_date <= CURRENT_DATE 
                AND CURRENT_DATE - end_date <= INTERVAL '1' DAY
            LIMIT 1;
            """, [player_id])
        # fmt: on
        result_row = cursor.fetchone()

    if result_row is None:
        return Streak(days=0, start_date=timezone.now())

    return Streak(result_row[0], result_row[1])


def get_player_badges(player_id: int, day_limit: int = 0) -> List[NamedTuple]:
    with connection.cursor() as cursor:
        # fmt: off
        badges = cursor.execute(
            """
            WITH ved as (
                SELECT 
                    SUM(
                        1.0 * vegan_meal_count / total_meal_count
                        ) as vegan_equiv_days
                FROM game_daily_meal_log
                INNER JOIN (
                    SELECT MAX(rec_date) as r_date FROM game_daily_meal_log
                    WHERE player_id = %s
                        AND total_meal_count <> 0 
                        AND vegan_meal_count <= total_meal_count
                        AND CAST(rec_date as DATE) <= CURRENT_DATE - %s
                    GROUP BY CAST(rec_date as DATE)
                ) maxd ON maxd.r_date = rec_date
            ), badges_with_earned as (
                SELECT 
                    gb.id,
                    gb.name,
                    gb.tier,
                    gb.description,
                    gb.image,
                    gb.measurable_threshold_to_earn,
                    unit.name unit,
                    gm.rate_per_vegan_day * (SELECT coalesce(max(vegan_equiv_days), 0) FROM ved)
                        as progress,  
                    gm.rate_per_vegan_day * (SELECT coalesce(max(vegan_equiv_days), 0) FROM ved) 
                        >= gb.measurable_threshold_to_earn as earned
                FROM game_badge as gb
                JOIN game_measurable gm ON gb.measurable_id = gm.id
                JOIN game_measurableunit unit ON gm.unit_id = unit.id 
                ORDER BY gb.name, tier
            ), curr_tier_in_bname_grp as (
                SELECT b.id, b.name, b.tier FROM badges_with_earned as b
                JOIN (
                    SELECT name, 
                    MIN(b_tmp.tier) as tier
                    FROM badges_with_earned as b_tmp
                    WHERE NOT b_tmp.earned
                    GROUP BY name  
                ) b1 
                ON b.name = b1.name 
                AND b.tier = b1.tier
            )
            SELECT b.*,
                (CASE WHEN EXISTS (
                    SELECT FROM curr_tier_in_bname_grp as ctb 
                        WHERE b.id = ctb.id) 
                    THEN TRUE 
                    ELSE FALSE END) as is_current_tier_in_badge_grp 
            FROM badges_with_earned as b 
            ORDER BY b.tier, b.earned DESC
            """, [player_id, day_limit])
        # fmt: on
        desc = cursor.description
        badge = namedtuple("Badge", [col[0] for col in desc])
        return [badge(*row) for row in cursor.fetchall()]


@dataclass
class CommunalMeasurable:
    name: str
    progress: float
    unit: str
    emoji: str = ""


def add_emoji_to_community_measurable(measurable: CommunalMeasurable):
    # emoji charset for reference: https://www.w3schools.com/charsets/ref_emoji.asp
    animal_emojis = ["🐏", "🐐", "🐑", "🐓", "🐔", "🐖", "🐟", "🐠", "🐄"]
    carbon_emission_emojis = ["🌬️", "🌍", "🌎", "🌏"]
    forest_area_emojis = ["🌱", "🌲", "🌳", "🌴"]
    water_usage_emojis = ["🚿", "🛀", "🛁", "🚰", "🌊"]
    match measurable.name:
        case "Animal":
            measurable.emoji = random.choice(animal_emojis)
        case "Carbon Emission":
            measurable.emoji = random.choice(carbon_emission_emojis)
        case "Forest Area":
            measurable.emoji = random.choice(forest_area_emojis)
        case "Water Usage":
            measurable.emoji = random.choice(water_usage_emojis)


def get_communal_measurables() -> List[CommunalMeasurable]:
    with connection.cursor() as cursor:
        # fmt: off
        cmeasurables = cursor.execute("""
            WITH ved as (
                SELECT 
                    ROUND(SUM(1.0 * vegan_meal_count / total_meal_count), 4) 
                        as vegan_equiv_days
                FROM game_daily_meal_log
                INNER JOIN (
                    SELECT MAX(rec_date) as r_date FROM game_daily_meal_log
                        WHERE total_meal_count <> 0 
                        AND vegan_meal_count <= total_meal_count
                        AND CAST(rec_date as DATE) <= CURRENT_DATE
                    GROUP BY CAST(rec_date as DATE), player_id
                ) maxd ON maxd.r_date = rec_date )
            SELECT gm.name, 
                ROUND(rate_per_vegan_day * (SELECT max(coalesce(vegan_equiv_days,0)) FROM ved)) as progress, 
                unit.name as unit
            FROM game_measurable as gm
            JOIN game_measurableunit unit ON gm.unit_id = unit.id 
            ORDER BY gm.name
            ;
        """)              
        # fmt: on
        cmeasurables = []
        for row in cursor.fetchall():
            cm = CommunalMeasurable(name=row[0], progress=row[1], unit=row[2])
            add_emoji_to_community_measurable(cm)
            cmeasurables.append(cm)
    return cmeasurables
