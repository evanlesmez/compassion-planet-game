from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string
from typing import List, Iterable
from django.utils import timezone
from game.models import Player
from game.queries import (
    get_communal_measurables,
    CommunalMeasurable,
    get_fully_vegan_streak,
    get_partial_vegan_streak,
    Streak,
)
from accounts.tokens import unsub_email_token_gen, models
from django.conf import settings
from django.urls import reverse


def gen_unsub_link(user: models.AbstractUser):
    rel_url = reverse(
        "unsub_email", args=(user.id, unsub_email_token_gen.make_token(user))
    )
    return f"{settings.EMAIL_PAGE_DOMAIN}{rel_url}"


@shared_task
def debug(arg):
    print(arg)


def send_log_meals_mail_to(players: Iterable[Player]):
    communal_measurables: List[CommunalMeasurable] = get_communal_measurables()
    for p in players:
        fully_v_streak: Streak = get_fully_vegan_streak(player_id=p.id)
        partial_v_streak: Streak = get_partial_vegan_streak(player_id=p.id)
        context = {
            "player": p,
            "fully_v_streak": fully_v_streak,
            "partial_v_streak": partial_v_streak,
            "communal_measurables": communal_measurables,
            "unsub_link": gen_unsub_link(p.user),
        }
        # TODO needed delay for mass sending verification email probably due to builtin threading
        # send_mail here should block properly but be mindful
        send_mail(
            subject="Log today's meals",
            message=render_to_string("game/mail/log_meals_reminder.txt", context),
            html_message=render_to_string("game/mail/log_meals_reminder.html", context),
            recipient_list=[p.user.email],
            from_email=None,
        )


@shared_task
def send_log_meals_mail():
    players = (
        Player.objects.filter(user__is_email_verified=True)
        .filter(user__is_subscribed_meal_log_mail=True)
        .exclude(dailymeallog__rec_date__date=timezone.now().date())
    )
    send_log_meals_mail_to(players)


# TODO test on fail. Need a way to notify team which mail bounced
