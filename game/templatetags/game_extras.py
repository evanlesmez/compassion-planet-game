from django import template
import math
import inflect
from django.core import serializers
from django.conf import settings

register = template.Library()
inflect_engine = inflect.engine()


@register.filter(name="pluralize_noun")
def pluralize_noun(value, arg) -> str:
    return inflect_engine.plural_noun(value, arg)


@register.filter(name="floor")
def floor(value) -> int:
    return math.floor(value)


# required double parsing after json_script so unused for now b/c unsure of implications
@register.filter(name="jsonify_queryset", safe=True)
def jsonify_queryset(qs):
    return serializers.serialize("json", qs)


@register.filter(name="qs_to_list")
def qs_to_list(qs):
    return list(qs.values())


@register.simple_tag
def email_page_domain():
    return settings.EMAIL_PAGE_DOMAIN
