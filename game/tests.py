from game.models import Badge, Measurable, MeasurableUnit
from django.test import TestCase
from assertpy import assert_that


def create_unit(name):
    return MeasurableUnit.objects.create(name=name)


def create_measurable(unit, rate_per_vegan_day=1, name="Measurey Measurable"):
    return Measurable.objects.create(
        name=name, rate_per_vegan_day=rate_per_vegan_day, unit=unit
    )


def create_badge(
    measurable, measurable_threshold_to_earn=100, tier=1, name="Badgey Badge"
):
    return Badge.objects.create(
        name=name,
        measurable=measurable,
        description="A badge for testing",
        tier=tier,
        measureable_threshold_to_earn=100,
    )


class UnitModelTest(TestCase):
    def test_basic(self):
        unit = create_unit("kg")
        assert_that(unit.name).is_equal_to("kg")
