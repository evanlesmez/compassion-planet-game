from dataclasses import dataclass
import pytest
from assertpy import assert_that
from game import tasks
from game.models import User, Player, DailyMealLog
from unittest.mock import patch, create_autospec
from dataclasses import dataclass
from typing import Optional
from unittest.mock import patch
from django.core import mail


@dataclass()
class CreatableUserPlayer:
    username: str
    email: str
    password: str = "password"
    is_email_verified: bool = False
    is_subscribed_meal_log_mail: bool = True
    p_avatar: Optional[str] = None


#! Must mock postgres queries for sqlite in memory database for SQL syntax difference
@pytest.mark.django_db
@patch("game.tasks.get_fully_vegan_streak")
@patch("game.tasks.get_partial_vegan_streak")
def test_send_log_meals_mail(mock_get_fv_streak, mock_get_pv_streak):
    mock_get_fv_streak.side_effect = [0, 0, 1, 3, 7]
    mock_get_pv_streak.side_effect = [0, 1, 1, 6, 7]
    user_players = [
        CreatableUserPlayer("bulbasaur", "bulba@pokeball.com", is_email_verified=True),
        CreatableUserPlayer("charmander", "char@pokeball.com", is_email_verified=True),
        CreatableUserPlayer("pikachu", "pika@pokeball.com"),
        CreatableUserPlayer(
            "squirtle",
            "squir@pokeball.com",
            is_email_verified=True,
            is_subscribed_meal_log_mail=False,
        ),
        CreatableUserPlayer("sandshrew", "sandy@pokeball.com", is_email_verified=True),
    ]

    for up in user_players:
        user = User.objects.create(
            username=up.username,
            password=up.password,
            email=up.email,
            is_email_verified=up.is_email_verified,
            is_subscribed_meal_log_mail=up.is_subscribed_meal_log_mail,
        )
        player = Player.objects.create(user=user)
        # TODO add freezegun to freeze datetime to be safe if running at UTC midnight
        if up == user_players[-1]:
            DailyMealLog.objects.create(
                player=player, total_meal_count=3, vegan_meal_count=3
            )

    tasks.send_log_meals_mail()
    assert_that(mail.outbox).is_length(2)
    for i in range(2):
        assert_that(mail.outbox[i].to[0]).is_equal_to(user_players[i].email)

    # TODO In both plain txt and html
    # TODO assert that link to meal log button exists in body with proper url
    # TODO assert that unsub link is in body
