from django.urls import path
from . import views

app_name = "game"
urlpatterns = [
    path("", views.index, name="index"),
    path("player/<int:player_id>/", views.player, name="player"),
    path(
        "player/<int:player_id>/daily_meal_log",
        views.daily_meal_log,
        name="daily_meal_log",
    ),
    path("player/<int:player_id>/feedback", views.feedback, name="feedback"),
    path("starter_animal", views.select_starter_animal, name="starter_animal"),
    path("library", views.Library.as_view(), name="library"),
]
