from django.shortcuts import render, get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.utils import timezone
from django.views.generic import TemplateView
from typing import List
import itertools
from .models import Badge, DailyMealLog, Player, PlayerFeedback, Avatar
from .forms import DailyMealLogForm, PlayerFeedbackForm
from .queries import (
    get_fully_vegan_streak,
    get_partial_vegan_streak,
    get_player_badges,
    Streak,
    get_communal_measurables,
    CommunalMeasurable,
)
from typing import List


def index(request):
    communal_measurables: List[CommunalMeasurable] = get_communal_measurables()
    return render(
        request, "game/index.html", {"communal_measurables": communal_measurables}
    )


@login_required
def player(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    fully_v_streak: Streak = get_fully_vegan_streak(player_id=player.id)
    partial_v_streak: Streak = get_partial_vegan_streak(player_id=player.id)
    badges = get_player_badges(player_id=player.id)
    previous_badges = get_player_badges(player_id=player.id, day_limit=1)
    communal_measurables: List[CommunalMeasurable] = get_communal_measurables()
    try:
        latest_daily_meal_log = DailyMealLog.objects.filter(
            player=player, rec_date__lte=timezone.now()
        ).latest("rec_date")

    except ObjectDoesNotExist:
        latest_daily_meal_log = DailyMealLog(
            player=player, rec_date=timezone.datetime(2000, 1, 1)
        )
    badge_iter = itertools.groupby(badges, key=lambda b: b.tier)

    grouped_badges = {}
    for key, grp in badge_iter:
        if key in grouped_badges:
            grouped_badges[key] = grouped_badges[key] + list(grp)
            continue
        grouped_badges[key] = list(grp)
    newly_earned_badges = get_newly_earned_badges(previous_badges, badges)
    return render(
        request,
        "game/player.html",
        {
            "player": player,
            "fully_v_streak": fully_v_streak,
            "partial_v_streak": partial_v_streak,
            "grouped_badges": grouped_badges,
            "newly_earned_badges": newly_earned_badges,
            "communal_measurables": communal_measurables,
            "latest_daily_meal_log": latest_daily_meal_log,
        },
    )


def find(pred, iterable):
    for element in iterable:
        if pred(element):
            return element
    return None


def get_newly_earned_badges(previous_badges, new_badges):
    newly_earned_badges = []
    for new_badge in new_badges:
        previous_badge = find(lambda x: x.id == new_badge.id, previous_badges)
        is_earned_now = previous_badge.earned == False and new_badge.earned == True
        if is_earned_now:
            newly_earned_badges.append(new_badge)
    return newly_earned_badges


@login_required
def daily_meal_log(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    if player.user != request.user:
        resp = HttpResponse()
        resp.status_code = 403
        return resp

    fully_v_streak: Streak = get_fully_vegan_streak(player_id=player.id)
    partial_v_streak: Streak = get_partial_vegan_streak(player_id=player.id)
    try:
        daily_meal_log = DailyMealLog.objects.filter(
            player=player, rec_date__lte=timezone.now()
        ).latest("rec_date")

    except ObjectDoesNotExist:
        daily_meal_log = DailyMealLog(rec_date=timezone.datetime(2000, 1, 1))

    if request.method == "POST":
        method = request.POST.get("_method", "").lower()
        if method == "put":
            form = DailyMealLogForm(request.POST, instance=daily_meal_log)
            success_msg = "Meal log updated"

        else:
            daily_meal_log = DailyMealLog(player=player)
            form = DailyMealLogForm(request.POST, instance=daily_meal_log)
            success_msg = "Meals logged"

        if form.is_valid():
            form.save()
            messages.success(request, success_msg)
            return HttpResponseRedirect(reverse("game:player", args=(player.id,)))

    else:
        if daily_meal_log.was_recorded_today:
            form = DailyMealLogForm(instance=daily_meal_log)

        else:
            form = DailyMealLogForm()

    return render(
        request,
        "game/daily_meal_log.html",
        {
            "form": form,
            "player_id": player.id,
            "partial_v_streak": partial_v_streak,
            "fully_v_streak": fully_v_streak,
        },
    )


@login_required
def select_starter_animal(request):
    if request.method == "POST":
        player = Player.objects.get(user=request.user)
        avatar = get_object_or_404(Avatar, id=request.POST["avatar"])

        player.avatar = avatar
        player.save()
        return HttpResponseRedirect(
            reverse(
                "game:index",
            )
        )

    avatars = Avatar.objects.all()
    return render(request, "game/starter_animal.html", {"avatars": avatars})


@login_required
def feedback(request, player_id):
    player = get_object_or_404(Player, pk=player_id)
    if player.user != request.user:
        resp = HttpResponse()
        resp.status_code = 403
        return resp

    if request.method == "POST":
        player_feedback = PlayerFeedback(player=player)
        form = PlayerFeedbackForm(request.POST, instance=player_feedback)
        if form.is_valid():
            form.save()
            messages.success(request, "Thanks for the feedback!")
            return HttpResponseRedirect(reverse("game:player", args=(player.id,)))
    else:
        form = PlayerFeedbackForm()

    return render(
        request,
        "game/player_feedback.html",
        {"form": form, "player_id": player.id},
    )


class Library(TemplateView):
    template_name = "game/library.html"
