pg_dump -U postgres compassion_game \
      --exclude-table-data 'game_user'  \
      --exclude-table-data 'game_player' \
      --exclude-table-data 'game_playerfeedback' \
      --exclude-table-data 'game_daily_meal_log' \
      --exclude-table-data 'django_admin_log' > ../backup/compassion_game_bak
